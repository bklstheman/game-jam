//
//  BertoMonster.swift
//  GameJam
//
//  Created by Alberto Lopez on 8/8/15.
//  Copyright (c) 2015 Team Mobile. All rights reserved.
//

import SpriteKit

class BertoMonster : SKSpriteNode {
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder) is not used in this app")
    }
    
    init(position: CGPoint) {
        let texture = SKTexture(imageNamed: "hero-right-step-right")
        super.init(texture: texture, color: nil, size: CGSizeMake(texture.size().width * 3, texture.size().width * 3))
        
       
        self.position = position
   
    }
}