//
//  Level2.swift
//  GameJam
//
//  Created by William Kluss on 8/9/15.
//  Copyright (c) 2015 Team Mobile. All rights reserved.
//

import SpriteKit

class Level2: SKScene, SKPhysicsContactDelegate {
    var hero:HeroNode!
    var controller: Controller = Controller()
    
    override func didMoveToView(view: SKView) {
        self.physicsWorld.contactDelegate = self
        
        self.updateWallImage()
        self.setupBackground()
        self.setupDoors()
        
        let borderBody = SKPhysicsBody(edgeLoopFromRect: self.frame)
        borderBody.friction = 0
        self.physicsBody = borderBody
        
        self.hero = createHero()
        
        self.addChild(self.hero)
        
        physicsWorld.gravity = CGVectorMake(0, 0)
        
        self.controller = Controller(position: CGPoint(x: 150, y: 150))
        self.addChild(self.controller)

        self.setupAndStartTimer()

    }
    
    override func update(currentTime: NSTimeInterval) {
        if hero.destination != nil {
            hero.walk(self.controller.pressedDirection())
        }
    }
    
    func createHero() -> HeroNode {
        let hero = HeroNode()
        hero.name = "hero"
        //TODO: Change position to something better
        hero.position = CGPoint(x: 107, y: 1070)
        hero.zPosition = 10
        
        //physics
        hero.physicsBody = SKPhysicsBody(circleOfRadius: hero.size.width / 2)
        hero.physicsBody?.affectedByGravity = false
        hero.physicsBody?.dynamic = true
        hero.physicsBody?.categoryBitMask = CollisionCategory.Hero.rawValue
        hero.physicsBody?.contactTestBitMask = CollisionCategory.Berto.rawValue | CollisionCategory.Monster.rawValue | CollisionCategory.Wall.rawValue | CollisionCategory.Shield.rawValue | CollisionCategory.Trap.rawValue
        
        // lighting
        hero.lightingBitMask = 1
        hero.shadowCastBitMask = 1
        
        return hero
    }
    
    func updateWallImage() {
        for wall in self.children as! [SKSpriteNode] {
            if wall.name == "wall" {
                wall.texture = SKTexture(imageNamed: "wall-grey")
                wall.physicsBody = SKPhysicsBody(rectangleOfSize: wall.size)
                wall.physicsBody?.dynamic = false
                wall.physicsBody?.affectedByGravity = false
                wall.physicsBody?.allowsRotation = false
            }
        }
    }
    
    func setupBackground() {
        let background = self.childNodeWithName("background") as! SKSpriteNode
        
        background.texture = SKTexture(imageNamed: "background2")
    }
    
    func setupDoors() {
        let door = self.childNodeWithName("door") as! SKSpriteNode
        
        let newDoor = DoorNode()
        newDoor.position = door.position
        newDoor.size = door.size
        newDoor.zRotation = door.zRotation
        newDoor.zPosition = door.zPosition
        
        newDoor.physicsBody = SKPhysicsBody(texture: SKTexture(imageNamed: "door"), size: door.size)
        newDoor.physicsBody?.categoryBitMask = CollisionCategory.Door.rawValue
        newDoor.physicsBody?.dynamic = false
        
        door.removeFromParent()
        self.addChild(newDoor)
        
        
        let fakeDoor = self.childNodeWithName("fakeDoor") as! SKSpriteNode
        
        let newFakeDoor = DoorNode()
        newFakeDoor.position = fakeDoor.position
        newFakeDoor.size = fakeDoor.size
        newFakeDoor.zRotation = fakeDoor.zRotation
        newFakeDoor.zPosition = fakeDoor.zPosition
        
        newFakeDoor.physicsBody = SKPhysicsBody(texture: SKTexture(imageNamed: "door"), size: door.size)
        newFakeDoor.physicsBody?.categoryBitMask = CollisionCategory.Door.rawValue
        newFakeDoor.physicsBody?.dynamic = false
        
        fakeDoor.removeFromParent()
        self.addChild(newFakeDoor)
    }

    func setupAndStartTimer() {
        
        let titleBar = self.childNodeWithName("topbar") as! SKSpriteNode
        let timerLabel = titleBar.childNodeWithName("timer") as! SKLabelNode
        let startingTime: Int = 30
        timerLabel.zPosition = 15
        
        var countDownInt = startingTime
        let updateLabelAction = SKAction.runBlock { () -> Void in
            timerLabel.text = "Timer: \(countDownInt)"
            --countDownInt
        }
        
        let waitAction = SKAction.waitForDuration(1.0)
        let updateAndWait = SKAction.sequence([updateLabelAction, waitAction])
        
        self.runAction(SKAction.repeatAction(updateAndWait, count: startingTime), completion: { () -> Void in
            timerLabel.text = "Time's Up"
            let gameOverScene = StartScene(size: self.size, message: "Game Over", sceneName: "Level2")
            let reveal = SKTransition.flipHorizontalWithDuration(0.5)
            self.view?.presentScene(gameOverScene, transition: reveal)
        })
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        
        var touch  = touches.first as! UITouch
        let touchLocation = touch.locationInNode(self)
        self.hero.destination = touchLocation
        
        for button in self.controller.buttons as [ControllerButton] {
            if button.hitbox!(touchLocation) && find(self.controller.pressedButtons, button) == nil {
                self.controller.pressedButtons.append(button)
            }
        }
    }
    
    override func touchesMoved(touches: Set<NSObject>, withEvent event: UIEvent) {
        var touch  = touches.first as! UITouch
        
        let location = touch.locationInNode(self)
        let previousLocation = touch.previousLocationInNode(self)
        
        self.hero.destination = location
        
        for button in self.controller.buttons  {
            if button.hitbox!(previousLocation) && !button.hitbox!(location) {
                let index = find(self.controller.pressedButtons, button)
                if (index != nil) {
                    self.controller.pressedButtons.removeAtIndex(index!)
                }
            } else if !button.hitbox!(previousLocation) && button.hitbox!(location) && find(self.controller.pressedButtons, button) == nil{
                self.controller.pressedButtons.append(button)
            }
        }
    }
    
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        var touch  = touches.first as! UITouch
        let location = touch.locationInNode(self)
        let previousLocation = touch.previousLocationInNode(self)
        
        self.hero.destination = nil
        
        for button in self.controller.buttons {
            if button.hitbox!(previousLocation) {
                let index = find(self.controller.pressedButtons, button)
                if (index != nil) {
                    self.controller.pressedButtons.removeAtIndex(index!)
                }
            } else if !button.hitbox!(previousLocation) {
                if let index = find(self.controller.pressedButtons, button) {
                    self.controller.pressedButtons.removeAtIndex(index)
                }
            }
        }
    }
}
