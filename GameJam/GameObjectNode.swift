//
//  GameObjectNode.swift
//  GameJam
//
//  Created by William Kluss on 8/8/15.
//  Copyright (c) 2015 Team Mobile. All rights reserved.
//

import SpriteKit

let HeroCategoryName = "hero"

class GameObjectNode: SKSpriteNode {
    
    override init(texture: SKTexture!, color: UIColor!, size: CGSize) {
        super.init(texture: texture, color: color, size: size)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func moveYPostion(yPosition: CGFloat) {
        self.position.y += yPosition
    }
    
    func moveXPosition(xPosition: CGFloat) {
        self.position.x += xPosition
    }
}

let DoorCategory: UInt32 = 0x05
let FakeDoorCategory: UInt32 = 0x06
class DoorNode: GameObjectNode {
    init () {
        let texture = SKTexture(imageNamed: "door")
        super.init(texture: texture, color: UIColor.clearColor(), size: texture.size())
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

enum CharacterDirection : Int {
    case Up
    case Left
    case Down
    case Right
}

class HeroNode: GameObjectNode {
    
    let defaultMovement:CGFloat = 10.0
    var movement: CGFloat = 0
    
    var tick = 0
    var lastDirection: CharacterDirection?
    var currentDirection: CharacterDirection?
    var destination: CGPoint?
    var flashlight: SKSpriteNode!
    var shield: Float!
    
    init () {
        let texture = SKTexture(imageNamed: "hero-front")
        super.init(texture: texture, color: UIColor.clearColor(), size: CGSizeMake(140,140))
        self.movement = self.defaultMovement
        self.shield = 0
        lastDirection = .Down
        
        // add light emitter
        let light = SKLightNode()
        light.zPosition = 10
        light.categoryBitMask = 1
        light.falloff = 1.5
        light.position = CGPointZero
        light.ambientColor = UIColor.orangeColor()
        light.lightColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.5)
        light.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.99)
        
        flashlight = SKSpriteNode(imageNamed: "flashlight", normalMapped: false)
        flashlight.shadowCastBitMask = 1
        flashlight.lightingBitMask = 1
        flashlight.zRotation = 0
        flashlight.zPosition = 1
        flashlight.physicsBody = SKPhysicsBody(circleOfRadius: 5)
        flashlight.physicsBody?.dynamic = true
        flashlight.physicsBody?.pinned = true
        flashlight.addChild(light)
        
        self.addChild(flashlight)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func walk(directions: [ButtonDirection]) {
        var walkingSpeed: CGFloat = 5.0
        
        if find(directions, .Up) != nil {
            self.position.y += walkingSpeed
            if currentDirection == nil || directions.count == 1 {
                currentDirection = .Up
                flashlight.zRotation = CGFloat(M_PI)
            }
        }
        if find(directions, .Left) != nil {
            self.position.x -= walkingSpeed
            if currentDirection == nil || directions.count == 1 {
                currentDirection = .Left
                flashlight.zRotation = -CGFloat(M_PI_2)
            }
        }
        if find(directions, .Down) != nil {
            self.position.y -= walkingSpeed
            if currentDirection == nil || directions.count == 1 {
                currentDirection = .Down
                flashlight.zRotation = 0
            }
        }
        if find(directions, .Right) != nil {
            self.position.x += walkingSpeed
            if currentDirection == nil || directions.count == 1 {
                currentDirection = .Right
                flashlight.zRotation = CGFloat(M_PI_2)
            }
        }
        
        if currentDirection != nil {
            lastDirection = currentDirection
        }
        
        if !directions.isEmpty && self.currentDirection != nil {
            switch currentDirection! {
            case .Up:
                if tick % 20 < 10 {
                    self.texture = SKTexture(imageNamed: "hero-back-step-left")
                }
                else {
                    self.texture = SKTexture(imageNamed: "hero-back-step-right")
                }
            case .Left:
                if tick % 20 < 10 {
                    self.texture = SKTexture(imageNamed: "hero-left-step-left")
                }
                else {
                    self.texture = SKTexture(imageNamed: "hero-left-step-right")
                }
            case .Down:
                if tick % 20 < 10 {
                    self.texture = SKTexture(imageNamed: "hero-front-step-left")
                }
                else {
                    self.texture = SKTexture(imageNamed: "hero-front-step-right")
                }
            case .Right:
                if tick % 20 < 10 {
                    self.texture = SKTexture(imageNamed: "hero-right-step-left")
                }
                else {
                    self.texture = SKTexture(imageNamed: "hero-right-step-right")
                }
            }
        }
        else {
            currentDirection = nil
        }
        
        tick++
        if tick > 60 {
            tick = 0
        }
    }
    
    func walkTowards(destination: CGPoint) {
        // if the finger is too close to the current position, nothing happens
        if abs(destination.x - self.frame.midX) < 4 && abs(destination.y - self.frame.midY) < 4 {
            return
        }
        
        let direction = CGPoint(x: destination.x - self.frame.midX, y: destination.y - self.frame.midY)
        let normalizedDirection = CGPoint(x: direction.x / (sqrt(direction.x * 2 + direction.y * 2)), y: direction.y / (sqrt(direction.x * 2 + direction.y * 2)))
        
        if direction.x > 0 && direction.y > 0 {
            if direction.x > direction.y {
                currentDirection = .Right
            }
            else {
                currentDirection = .Up
            }
        }
        else if direction.x < 0 && direction.y > 0 {
            if -direction.x > direction.y {
                currentDirection = .Left
            }
            else {
                currentDirection = .Up
            }
        }
        else if direction.x > 0 && direction.y < 0 {
            if direction.x > -direction.y {
                currentDirection = .Right
            }
            else {
                currentDirection = .Down
            }
        }
        else if direction.x < 0 && direction.y < 0 {
            if -direction.x > -direction.y {
                currentDirection = .Left
            }
            else {
                currentDirection = .Down
            }
        }
        
        if currentDirection != nil {
            lastDirection = currentDirection
        }
        
        self.position.x += normalizedDirection.x * movement
        self.position.y += normalizedDirection.y * movement
        
        if self.currentDirection != nil {
            switch currentDirection! {
            case .Up:
                if tick % 20 < 10 {
                    self.texture = SKTexture(imageNamed: "hero-back-step-left")
                }
                else {
                    self.texture = SKTexture(imageNamed: "hero-back-step-right")
                }
            case .Left:
                if tick % 20 < 10 {
                    self.texture = SKTexture(imageNamed: "hero-left-step-left")
                }
                else {
                    self.texture = SKTexture(imageNamed: "hero-left-step-right")
                }
            case .Down:
                if tick % 20 < 10 {
                    self.texture = SKTexture(imageNamed: "hero-front-step-left")
                }
                else {
                    self.texture = SKTexture(imageNamed: "hero-front-step-right")
                }
            case .Right:
                if tick % 20 < 10 {
                    self.texture = SKTexture(imageNamed: "hero-right-step-left")
                }
                else {
                    self.texture = SKTexture(imageNamed: "hero-right-step-right")
                }
            }
        }
        else {
            currentDirection = nil
        }
        
        tick++
        if tick > 60 {
            tick = 0
        }
    }
    
    func moveByX(x: CGFloat, y: CGFloat) {
        var nextPosition = self.position
        nextPosition.x += x
        nextPosition.y += y
        if let context = self.parent as? GameScene {
            self.position = nextPosition
        }
    }
}

