//
//  Monster.swift
//  GameJam
//
//  Created by Mark P on 8/9/15.
//  Copyright (c) 2015 Team Mobile. All rights reserved.
//

import SpriteKit

class Monster: GameObjectNode {
    var facing: CharacterDirection!
    var pacing: CGFloat!
    var duration: NSTimeInterval!
    
    override init(texture: SKTexture!, color: UIColor!, size: CGSize) {
        super.init(texture: texture, color: color, size: size)
        facing = CharacterDirection.Down
        pacing = 50
        duration = 1.0
        physicsBody = SKPhysicsBody(texture: texture, size: texture.size())
        physicsBody?.dynamic = true
        physicsBody?.categoryBitMask = CollisionCategory.Monster.rawValue
        physicsBody?.collisionBitMask = CollisionCategory.Wall.rawValue
        runAction(SKAction.repeatActionForever(SKAction.moveBy(CGVectorMake(0, -pacing), duration: duration)))
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func changeDirection(direction: CharacterDirection!) {
        var action: SKAction!
        switch direction as CharacterDirection {
        case .Down:
            action = SKAction.moveBy(CGVectorMake(0, -pacing), duration: duration)
        case .Up:
            action = SKAction.moveBy(CGVectorMake(0, pacing), duration: duration)
        case .Left:
            action = SKAction.moveBy(CGVectorMake(-pacing, 0), duration: duration)
        case .Right:
            action = SKAction.moveBy(CGVectorMake(pacing, 0), duration: duration)
        default:
            fatalError("no direction to move to")
        }
        
        runAction(SKAction.repeatActionForever(action))
    }
}