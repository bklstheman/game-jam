//
//  GameScene.swift
//  GameJam
//
//  Created by William Kluss on 8/5/15.
//  Copyright (c) 2015 Team Mobile. All rights reserved.
//

import SpriteKit

enum CollisionCategory: UInt32 {
    case Hero       = 0
    case Wall
    case Monster
    case Berto
    case Trap
    case Shield
    case Door
}

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    let HeroCategoryName = "hero"
    let WallCategoryName = "wall"
    let MonsterCategoryName = "monster"
    let BertoMonsterNodeCategoryName = "bertoMonster"
    let BackgroundMusicKey = "backgroundMusic"
    
    var controller: Controller = Controller()
    let berto = BertoMonster(position:CGPointMake(100, 300))
    var hero: HeroNode!
    var isMusicPlaying = false
    
    override func didMoveToView(view: SKView) {
        physicsWorld.contactDelegate = self
        
        let background = self.childNodeWithName("background") as! SKSpriteNode
        
        background.texture = SKTexture(imageNamed: "background1")
        
        //I dont think this is the right way to place this here but it works for now....
        self.controller = Controller(position: CGPoint(x: 150, y: 150))
        self.addChild(self.controller)
        
        let wall = childNodeWithName("wall") as! SKSpriteNode
        wall.physicsBody?.dynamic = true
        wall.physicsBody?.categoryBitMask = CollisionCategory.Wall.rawValue
        wall.physicsBody?.collisionBitMask = CollisionCategory.Monster.rawValue
        
        //TODO: Add different monsters.

        self.hero = createHero()

        self.addChild(hero)
        self.addChild(createDoor())
        self.spawnBerto()
        addChild(createBenefit(CGPointMake(70, 700)))
        addChild(createMonster(CGPointMake(600, 700)))
        
        self.setupAndStartTimer()
        
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        
        var touch  = touches.first as! UITouch
        let touchLocation = touch.locationInNode(self)
        self.hero.destination = touchLocation

        for button in self.controller.buttons as [ControllerButton] {
            if button.hitbox!(touchLocation) && find(self.controller.pressedButtons, button) == nil {
                self.controller.pressedButtons.append(button)
            }
        }
    }
    
    override func touchesMoved(touches: Set<NSObject>, withEvent event: UIEvent) {
        var touch  = touches.first as! UITouch
        
        let location = touch.locationInNode(self)
        let previousLocation = touch.previousLocationInNode(self)
        
        self.hero.destination = location
        
        for button in self.controller.buttons  {
            if button.hitbox!(previousLocation) && !button.hitbox!(location) {
                let index = find(self.controller.pressedButtons, button)
                if (index != nil) {
                    self.controller.pressedButtons.removeAtIndex(index!)
                }
            } else if !button.hitbox!(previousLocation) && button.hitbox!(location) && find(self.controller.pressedButtons, button) == nil{
                self.controller.pressedButtons.append(button)
            }
        }
    }
    
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        var touch  = touches.first as! UITouch
        let location = touch.locationInNode(self)
        let previousLocation = touch.previousLocationInNode(self)
        
        self.hero.destination = nil
        
        for button in self.controller.buttons {
            if button.hitbox!(previousLocation) {
                let index = find(self.controller.pressedButtons, button)
                if (index != nil) {
                    self.controller.pressedButtons.removeAtIndex(index!)
                }
            } else if !button.hitbox!(previousLocation) {
                if let index = find(self.controller.pressedButtons, button) {
                    self.controller.pressedButtons.removeAtIndex(index)
                }
            }
        }
    }
    
    override func update(currentTime: CFTimeInterval) {
        if hero.destination != nil {
            hero.walk(self.controller.pressedDirection())
        }
    }
    
    func setupAndStartTimer() {
        
        let titleBar = self.childNodeWithName("title") as! SKSpriteNode
        let timerLabel = titleBar.childNodeWithName("timer") as! SKLabelNode
        let startingTime: Int = 30
        
        var countDownInt = startingTime
        let updateLabelAction = SKAction.runBlock { () -> Void in
            timerLabel.text = "Timer: \(countDownInt)"
            --countDownInt
        }
        
        let waitAction = SKAction.waitForDuration(1.0)
        let updateAndWait = SKAction.sequence([updateLabelAction, waitAction])
        
        self.runAction(SKAction.repeatAction(updateAndWait, count: startingTime), completion: { () -> Void in
            timerLabel.text = "Time's Up"
            let gameOverScene = StartScene(size: self.size, message: "Game Over", sceneName:"GameScene")
            let reveal = SKTransition.flipHorizontalWithDuration(0.5)
            self.view?.presentScene(gameOverScene, transition: reveal)
        })
    }
    
    func createHero() -> HeroNode {
        let hero = HeroNode()
        hero.name = "hero"
        //TODO: Change position to something better
        hero.position = CGPoint(x: self.size.width/2, y: 1000)
        hero.zPosition = 10
        
        //physics
        hero.physicsBody = SKPhysicsBody(circleOfRadius: hero.size.width / 2)
        hero.physicsBody?.dynamic = true
        hero.physicsBody?.categoryBitMask = CollisionCategory.Hero.rawValue
        hero.physicsBody?.contactTestBitMask = CollisionCategory.Berto.rawValue | CollisionCategory.Monster.rawValue | CollisionCategory.Wall.rawValue | CollisionCategory.Shield.rawValue | CollisionCategory.Trap.rawValue
        
        // lighting
        hero.lightingBitMask = 1
        hero.shadowCastBitMask = 1
        
        return hero
    }
    
    func createDoor() -> DoorNode {
        let door = DoorNode()
        door.name = "door"
        door.position = CGPoint(x: self.size.width/2, y: 25.0)
        door.size = CGSizeMake(70, 70)
        door.zRotation = CGFloat(M_PI)
        door.zPosition = 1
        
        door.physicsBody = SKPhysicsBody(texture: SKTexture(imageNamed: "door"), size: door.size)
        door.physicsBody?.categoryBitMask = CollisionCategory.Door.rawValue
        door.physicsBody?.dynamic = false

        return door
    }
    
    func createBenefit(position: CGPoint!) -> Benefit {
        let benefit = Benefit()
        benefit.position = position
        
        return benefit
    }
    
    func createMonster(position: CGPoint!) -> Monster {
        let texture = SKTexture(imageNamed: "monster")
        let monster = Monster(texture: texture, color: UIColor.redColor(), size: texture.size())
        monster.position = position
        
        return monster
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        // 1. Create local variables for two physics bodies
        var firstBody: SKPhysicsBody
        var secondBody: SKPhysicsBody
        
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        } else {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        
        if firstBody.categoryBitMask == CollisionCategory.Hero.rawValue &&
            secondBody.categoryBitMask == CollisionCategory.Berto.rawValue {
            self.runAction(SKAction.playSoundFileNamed("hitWall.wav", waitForCompletion: true))
            println("Hero collided with Berto")

            let hero = firstBody.node as! HeroNode
            if hero.shield > 0 {
                println("hero got shield yo!")
                hero.shield = hero.shield - 1.0
            } else {
                self.runAction(SKAction.waitForDuration(1.0)) {
                    //put back in the starting point.
                    hero.position = CGPoint(x: self.size.width/2, y: 1100)
                }
            }
        }
        
        if firstBody.categoryBitMask == CollisionCategory.Hero.rawValue &&
            secondBody.categoryBitMask == CollisionCategory.Shield.rawValue {
            println("hero collided with shield")
            let hero = firstBody.node as! HeroNode
            let shield = secondBody.node as! Benefit
            hero.shield = hero.shield + shield.shielding
            self.runAction(SKAction.waitForDuration(1.0)) {
                println("removing benefit")
                // todo make it swirl before removing
                shield.removeFromParent()
            }
        }
        
        if firstBody.categoryBitMask == CollisionCategory.Hero.rawValue &&
            secondBody.categoryBitMask == CollisionCategory.Door.rawValue {
            println("Hero found the exit")
            let victorySound = SKAction.playSoundFileNamed("pickup.wav", waitForCompletion: true)
            self.runAction(victorySound, completion: nil)
                
                let gameOverScene = StartScene(size: self.size, message: "Level 2!", sceneName:"Level2")
                let reveal = SKTransition.flipHorizontalWithDuration(0.5)
                self.view?.presentScene(gameOverScene, transition: reveal)
                
                self.removeActionForKey("")
        }
        
        if (firstBody.categoryBitMask == CollisionCategory.Monster.rawValue &&
                secondBody.categoryBitMask == CollisionCategory.Wall.rawValue) ||
                (firstBody.categoryBitMask == CollisionCategory.Wall.rawValue &&
                secondBody.categoryBitMask == CollisionCategory.Monster.rawValue) {
            println("monster hit a wall")
            // move the monster to the opposite way
            let monster = firstBody.node as! Monster
            monster.changeDirection(CharacterDirection.Up)
        }
    }
    
    func spawnBerto() {
        
        berto.physicsBody =  SKPhysicsBody(rectangleOfSize: berto.frame.size)
        berto.physicsBody!.dynamic = false;
        berto.name = BertoMonsterNodeCategoryName
        berto.physicsBody?.categoryBitMask = CollisionCategory.Berto.rawValue
        berto.physicsBody?.contactTestBitMask = CollisionCategory.Hero.rawValue | CollisionCategory.Wall.rawValue
        
        let xPos = berto.position.x;
        let yPos = berto.position.y;
        
        //
        
        let moveHorizontal:SKAction = SKAction.moveToX((xPos + 600), duration: 3.0)
        moveHorizontal.timingMode = SKActionTimingMode.EaseOut
        
        let moveBack = SKAction.moveToX(xPos, duration: 1.0)
        moveBack.timingMode = SKActionTimingMode.EaseOut
        
        let wait = SKAction.waitForDuration(0.2)
        
        let forward = SKAction.sequence([moveHorizontal, wait])
        
        let back = SKAction.sequence([moveBack, wait])
        
        //
        
        let texturesFacingRight = [SKTexture(imageNamed: "hero-right-step-left"),
                                    SKTexture(imageNamed: "hero-right-step-right"),
                                    SKTexture(imageNamed: "hero-right-step-left"),
                                    SKTexture(imageNamed: "hero-right-step-right"),
                                    SKTexture(imageNamed: "hero-right-step-left")]
        
        let animationActionRight = SKAction.animateWithTextures(texturesFacingRight, timePerFrame: 0.5)
        
        let texturesFacingLeft = [SKTexture(imageNamed: "hero-left-step-left"),
                                    SKTexture(imageNamed: "hero-left-step-right"),
                                    SKTexture(imageNamed: "hero-left-step-left"),
                                    SKTexture(imageNamed: "hero-left-step-right"),
                                    SKTexture(imageNamed: "hero-left-step-left")]
        
        let animationActionLeft = SKAction.animateWithTextures(texturesFacingLeft, timePerFrame: 0.5)
        
        let moveRightGroup = SKAction.group([forward, animationActionRight])
        
        let moveLeftGroup = SKAction.group([back, animationActionLeft])
        
        let backAndForthGroups = SKAction.sequence([moveRightGroup, wait, moveLeftGroup, wait])
        
        let repeatBackAndForthGroups = SKAction.repeatActionForever(backAndForthGroups)
        
        //[SKAction group:[repeatBackAndForth, repeatAnimation]];
        //[sprite runAction:group];
        
//        // 3
//        berto.physicsBody?.friction = 0.0;
//        // 4
//        berto.physicsBody?.restitution = 1.0;
//        // 5
//        berto.physicsBody?.linearDamping = 0.0;
//        // 6
//        berto.physicsBody?.allowsRotation = false;
//        //berto.physicsBody?.affectedByGravity = false;
//        
//        berto.physicsBody?.mass = 0.02;
        self.addChild(berto)
        
//        let impulse = CGVectorMake(100.0,100.0);
//        berto.physicsBody?.applyImpulse(CGVectorMake(10.0, 10.0))
        berto.runAction(repeatBackAndForthGroups)
    }
}

class StartScene : SKScene {
    
    var message = "FlashLight!"
    var sceneName = "GameScene"
    
    init(size: CGSize, message: String, sceneName: String) {
        super.init(size: size)
        self.backgroundColor = SKColor.whiteColor()
        //let bgImage = SKSpriteNode(imageNamed:"checkerboard_grey.png")
        //bgImage.position = CGPointMake(self.size.width/2, self.size.height/2);
        //bgImage.size = size
        
        //self.addChild(bgImage)
        self.sceneName = sceneName
        
        let label = SKLabelNode(fontNamed: "Candleduster")
        label.text = message
        label.fontSize = 40
        label.fontColor = SKColor.blackColor()
        label.position = CGPointMake(self.size.width/2, self.size.height/1.1)
        
        let tapLabel = SKLabelNode(fontNamed: "Candleduster")
        tapLabel.text = "Tap to play"
        tapLabel.fontSize = 40
        tapLabel.fontColor = SKColor.blackColor()
        tapLabel.position = CGPointMake(self.size.width/2, self.size.height/1.5)
        
        self.physicsBody?.affectedByGravity = false
        
        self.addChild(label)
        self.addChild(tapLabel)
        self.addChild(fireButtonNode())
        self.addChild(self.createHero())
        
    }
    
    func fireButtonNode() ->SKSpriteNode{
        var fireNode = SKSpriteNode (imageNamed: "flashlightImage")
        
        fireNode.size = CGSizeMake(fireNode.size.width/2, fireNode.size.height/2)
        fireNode.position = CGPointMake(300,300);
        fireNode.name = "fireButtonNode";//how the node is identified later
        fireNode.zPosition = 1.0;
        fireNode.position = CGPointMake(self.size.width/2, self.size.height/1.3)
        
        return fireNode;
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        let touch: UITouch = touches.first as! UITouch
        let location = touch.locationInNode(self)
        let node = self.nodeAtPoint(location)
        
        self.runAction(SKAction.waitForDuration(1.0)){
            let reveal = SKTransition.flipHorizontalWithDuration(0.5)
            if (self.sceneName == "GameScene"){
                if let mainScene = GameScene.unarchiveFromFile(self.sceneName) as? GameScene{
                    self.view?.presentScene(mainScene, transition: reveal)
                }
            }else{
                if let secondScene = Level2.unarchiveFromFile("Level2") as? Level2{
                    self.view?.presentScene(secondScene, transition: reveal)
                }
            }
        }
        
    }
    
    func createHero() -> HeroNode {
        let hero = HeroNode()
        hero.name = "hero"
        //TODO: Change position to something better
        hero.position = CGPoint(x: self.size.width/2, y:25)
        hero.zPosition = 10
        hero.physicsBody?.affectedByGravity = false
        hero.currentDirection = .Up
        hero.flashlight.zRotation = CGFloat(M_PI)
        hero.texture = SKTexture(imageNamed: "hero-back-step-left")
        //physics
        hero.physicsBody = SKPhysicsBody(circleOfRadius: hero.size.width / 2)
        hero.physicsBody?.dynamic = false
        
        
        // lighting
        hero.lightingBitMask = 1
        hero.shadowCastBitMask = 1
        
        return hero
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
