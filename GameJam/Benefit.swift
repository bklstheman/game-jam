//
//  Benefit.swift
//  GameJam
//
//  Created by Mark P on 8/9/15.
//  Copyright (c) 2015 Team Mobile. All rights reserved.
//

import SpriteKit

class Benefit: GameObjectNode {
    var shielding: Float!
    var type: CollisionCategory!
    init() {
        let texture = SKTexture(imageNamed: "tank")
        super.init(texture: texture, color: UIColor.clearColor(), size: texture.size())
        shielding = 10
        type = CollisionCategory.Shield
        physicsBody = SKPhysicsBody(texture: texture, size: texture.size())
        physicsBody?.dynamic = true
        physicsBody?.categoryBitMask = type.rawValue
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}