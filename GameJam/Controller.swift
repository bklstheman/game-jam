//
//  Controller.swift
//  GameJam
//
//  Created by William Kluss on 8/8/15.
//  Copyright (c) 2015 Team Mobile. All rights reserved.
//

import SpriteKit


class Controller: SKSpriteNode {
    var buttons = [ControllerButton]()
    var pressedButtons = [ControllerButton]()
    
    let buttonUp = ControllerButton(imageNamed: "button-up", direction: .Up, position: CGPoint(x: 100, y: 150))
    let buttonLeft = ControllerButton(imageNamed: "button-left", direction: .Left, position: CGPoint(x: 50, y: 100))
    let buttonDown = ControllerButton(imageNamed: "button-down", direction: .Down, position: CGPoint(x: 100, y: 50))
    let buttonRight = ControllerButton(imageNamed: "button-right", direction: .Right, position: CGPoint(x: 150, y: 100))
    
    convenience init(position: CGPoint) {
        self.init(color: UIColor.clearColor(), size:CGSizeMake(300, 300))

        self.addChild(buttonUp)
        self.addChild(buttonLeft)
        self.addChild(buttonDown)
        self.addChild(buttonRight)
        
        self.buttons = [buttonDown, buttonLeft, buttonRight, buttonUp]
        
        let l = 94 as CGFloat
        let x0 = 90 as CGFloat
        let y0 = 100 as CGFloat
        let angle = CGFloat(tan(M_PI / 3))
        
        buttonUp.hitbox = {
            (location: CGPoint) -> Bool in
            return location.y - y0 > 0 && (abs(location.x - x0) <= abs(location.y - y0) * angle) && (pow((location.x - x0),2) + pow((location.y - y0),2) <= pow(l,2))
        }
        buttonLeft.hitbox = {
            (location: CGPoint) -> Bool in
            return location.x - x0 < 0 && (abs(location.x - x0) * angle >= abs(location.y - y0)) && (pow((location.x - x0),2) + pow((location.y - y0),2) <= pow(l,2))
        }
        buttonDown.hitbox = {
            (location: CGPoint) -> Bool in
            return location.y - y0 < 0 && (abs(location.x - x0) <= abs(location.y - y0) * angle) && (pow((location.x - x0),2) + pow((location.y - y0),2) <= pow(l,2))
        }
        buttonRight.hitbox = {
            (location: CGPoint) -> Bool in
            return location.x - x0 > 0 && (abs(location.x - x0) * angle >= abs(location.y - y0)) && (pow((location.x - x0),2) + pow((location.y - y0),2) <= pow(l,2))
        }
    }
    
    func pressedDirection() -> [ButtonDirection] {
        var buttonDirections = [ButtonDirection]()
        for button in self.pressedButtons as [ControllerButton] {
            buttonDirections.append(button.direction!)
        }
        return buttonDirections
    }
}
