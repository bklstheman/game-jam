//
//  ControllerButton.swift
//  GameJam
//
//  Created by William Kluss on 8/8/15.
//  Copyright (c) 2015 Team Mobile. All rights reserved.
//

import SpriteKit

enum ButtonDirection {
    case Up
    case Down
    case Left
    case Right
    case Unknown
}

class ControllerButton: SKSpriteNode {    
    var direction: ButtonDirection?
    var hitbox: (CGPoint -> Bool)?
    
    convenience init(imageNamed: String, direction: ButtonDirection, position: CGPoint) {
        self.init(imageNamed: imageNamed)
        self.direction = direction
        self.texture?.filteringMode = .Linear
        self.position = position
        self.zPosition = 10000;
        
        hitbox = {(location: CGPoint) -> Bool in
            return self.containsPoint(location)
        }
    }
}
